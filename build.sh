#!/usr/bin/env bash

declare -r TEMPLATE_FILE='index.html.template'
declare -a COMMAND_WITH_ARGUMENTS=('lscpu' 'uname -a' 'free -h')

yum update -y
yum install -y procps-ng

for cmd in "${COMMAND_WITH_ARGUMENTS[@]}"; do
    echo -n "<!DOCTYPE html>
    <html lang=\"$(echo ${LANG} | awk -F '_' '{printf $1}')\">
    <head>
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
        <title>Vercel Build Environment</title>
    </head>
    <body>
        <pre>
" > "public/${cmd}.html"
    ${cmd} >> "public/${cmd}.html"
    echo -n '
        </pre>
    </body>
</html>' >> "public/${cmd}.html"
done

# Update Build Info
sed -e "s|{{ BUILDDATE }}|<time>$(date -u --iso-8601='seconds')</time>|i" "${TEMPLATE_FILE}" > public/index.html
